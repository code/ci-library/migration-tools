# Migration Script

To migrate personal private projects, adjust `personal-private.env` file and from
command line type

```
$ ./migrate-repos.sh -o personal-private.env -t $API_TOKEN -v
```

NOTE: Personal's public repos cannot be migrated programmatically because API does
not support creating personal projects.

To migrate a full public project, check `qualcomm-public.env` as example, create one for your project and call it the same
way as above

```
$ ./migrate-repos.sh -o <project>.env -t $API_TOKEN -v
```

The `API_TOKEN` is obtained through a CLO API call where a `CLIENT_ID` and `CLIENT_SECRET` is required. If you
do not know these, contact [codelinaro-migration@linaro.org](mailto:codelinaro-migration@linaro.org) for further instructions.

```
export API_TOKEN="$(curl --silent \
                         --request POST \
                         --url https://auth.codelinaro.org/oauth/token \
                         --header 'content-type: application/json' \
                         --data '{"client_id":"<CLIENT ID>","client_secret":"<CLIENT_SECRET>","audience":"https://api.codelinaro.org/v1/","grant_type":"client_credentials"}' \
                         | jq -r '.access_token')"
```
