#!/usr/bin/env bash

set -o errexit
set -o pipefail

DEFAULT_GIT_PUSH_PARAMS="--all"
DEFAULT_ONLY_CREATE_REPO='false'

usage() {
    cat<<EOF
Usage : ./migrate-repos.sh -o <configuration file> -t <API token> -v
EOF
    exit 1
}

mirror () {
    local repo_path="$1"
    local vis="$2"

    local CLO_SRC="${CLO_SRC_SCHEME}${CLO_SRC_HOST}${CLO_SRC_PATH}"
    local CLO_DST="${CLO_DST_SCHEME}${CLO_DST_HOST}${CLO_DST_PATH}"

    # get sourced variables from
    local src="$CLO_SRC"
    local dst="$CLO_DST"
    local project_slug="${CLO_DST_PROJECT_SLUG:-$(basename $CLO_DST)}"
    local parent_group="${CLO_DST_PARENT_GROUP:-}"
    local api="${CLO_DST_API:-}"

    subgroups="$parent_group/$(dirname $repo_path)"

    # removing leading and trailing dot/slashes
    subgroups="${subgroups#/}"
    subgroups="${subgroups%.}"
    subgroups="${subgroups%/}"

    repo="$(basename $repo_path .git)"

    if [ -n "${api}" ]; then
    	# create subgroup through API
    	if [[ -n "$subgroups" ]]; then
    	    POST_SUBGROUPS="$(curl --silent \
    	         --request POST \
    	         --url $api/projects/$project_slug/git/subgroups \
    	         --header "Authorization: Bearer ${API_TOKEN}" \
    	         --header "content-type: application/json" \
    	         --data "{\"subGroups\":\"$subgroups\"}")"
    	    if [[ "${TRACE-0}" == "1" ]]; then
    	        echo "$POST_SUBGROUPS" | jq '.'
    	    fi
    	fi

    	# create git-repo under subgroups through API in case public visibility
    	# (if project is not created through the API, git-pushing will create and by default it is private)
    	if [[ "$vis" == "public" ]]; then
    	    POST_REPO="$(curl --silent \
    	         --request POST \
    	         --url $api/projects/$project_slug/git/repo \
    	         --header "Authorization: Bearer ${API_TOKEN}" \
    	         --header 'content-type: application/json' \
    	         --data "{\"name\":\"${repo}\",\"repoPathNew\":\"$subgroups\"}")"
    	    if [[ "${TRACE-0}" == "1" ]]; then
    	        echo "$POST_REPO" | jq '.'
    	    fi
	fi
    fi

    # there is no need mirror in case only repository creation is required
    if [ "${ONLY_CREATE_REPO}" = 'false' ]; then
    	# update or clone source repository
    	if [ -d "$repo_path" ]; then
    		echo "Pulling latest changes from $src/$repo_path"
    	    (cd $repo_path && git remote update)
    	else
    		echo "Cloning from $src/$repo_path"
    	    git clone --mirror $src/$repo_path $repo_path
    	fi

    	# dst_path considers sub-groups which in turn consider parent-group
    	dst_path="$repo"
    	if [[ -n "$subgroups" ]]; then
    	    dst_path="$subgroups/$repo"
    	fi
    	dst_path="${dst_path}.git"

    	# update destination repository
    	echo "Pushing latest changes to $dst/$dst_path"
    	(cd $repo_path && git push ${GIT_PUSH_PARAMS} $dst/$dst_path)
    fi

}

main() {
    # check if conf file really exists
    if [[ ! -f $CONF_FILE ]]; then usage; fi

    source $CONF_FILE

    vis='public'
    if [[ -n "$CLO_DST_REPOS_PUBLIC" && "$CLO_DST_REPOS_PUBLIC" == "false" ]]; then
        vis='private'
    fi

    # mirror all repos under $CLO_SRC_HOST/$CLO_SRC_PATH
    (
        mkdir -p $CLO_SRC_HOST/$CLO_SRC_PATH
        pushd $CLO_SRC_HOST/$CLO_SRC_PATH
        for repo_path in $CLO_SRC_REPO_PATHS;  do
            echo
            mirror "$repo_path" $vis
            echo
        done
        popd
    )
}

while getopts ":o:t:vh" o; do
    case "${o}" in
        o)
            CONF_FILE=${OPTARG}
            ;;
        t)
            API_TOKEN=${OPTARG}
            ;;
        v)
            set -o xtrace
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

# Command line argurments
CONF_FILE="${CONF_FILE:-}"
API_TOKEN="${API_TOKEN:-}"

# Environment variables
GIT_PUSH_PARAMS="${GIT_PUSH_PARAMS:-$DEFAULT_GIT_PUSH_PARAMS}"
ONLY_CREATE_REPO="${ONLY_CREATE_REPO:-$DEFAULT_ONLY_CREATE_REPO}"

main "$@"
